﻿namespace Todo.Entities
{
    public class TaskEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsDone { get; set; }
        public Priority Priority { get; set; }
    }
}