﻿USE TodoDatabase;
GO

DROP TABLE [dbo].[Task];
GO

CREATE TABLE [dbo].[Task]
(
	Id INT NOT NULL IDENTITY(1, 1),
	Title NVARCHAR(128) NOT NULL,
	IsDone BIT NOT NULL DEFAULT 0,
);
GO


SET IDENTITY_INSERT [dbo].[Task] ON;
INSERT INTO [dbo].[Task]([Id], [Title], [IsDone])
VALUES
(1, 'Do something good', 0),
(2, 'Do something bad', 1),
(3, 'Do nothing', 1),
(4, 'Finish home task', 0)
SET IDENTITY_INSERT [dbo].[Task] OFF;
GO

ALTER TABLE Task ADD [Priority] INT;
GO

UPDATE Task SET [Priority] = 1
GO
UPDATE [dbo].[Task] SET [Priority] = 2 WHERE [Id] = 2
GO
UPDATE [dbo].[Task] SET [Priority] = 3 WHERE [Id] = 3
GO
UPDATE [dbo].[Task] SET [Priority] = 4 WHERE [Id] = 4
GO

SELECT * FROM Task;
GO