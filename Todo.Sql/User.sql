DROP TABLE [User]
CREATE TABLE [dbo].[User]
(
	Id INT NOT NULL IDENTITY(1, 1),
	Name NVARCHAR(32) NOT NULL,
	Email NVARCHAR (64) NOT NULL,
	Pass NVARCHAR(64) NOT NULL
	CONSTRAINT pk_user_id PRIMARY KEY(Id)
);
GO

SET IDENTITY_INSERT [dbo].[User] ON;
INSERT INTO [dbo].[User](Id, Name, Email, Pass)
VALUES
(1, 'Tester1', 'tester1@mail.loc', 'test_pass'),
(2, 'Tester2', 'tester2@mail.loc', 'test_pass'),
(3, 'Tester3', 'tester3@mail.loc', 'test_pass'),
(4, 'Tester4', 'tester4@mail.loc', 'test_pass')
SET IDENTITY_INSERT [dbo].[User] OFF;