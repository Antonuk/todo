﻿namespace Todo.WebUI.Code.Service
{
    public interface IHashService
    {
        string Hash(string password);
    }
}
