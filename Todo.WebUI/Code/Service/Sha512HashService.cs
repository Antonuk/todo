﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security;
using System.Text;

namespace Todo.WebUI.Code.Service
{
    public class Sha512HashService : IHashService
    {
        public string Hash(string password)
        {
            if(string.IsNullOrEmpty(password))
            {
                return string.Empty;
            }
            else
            {
                using (var sha512 = new System.Security.Cryptography.SHA512Managed())
                {
                    byte[] textData = Encoding.UTF8.GetBytes(password);
                    byte[] hash = sha512.ComputeHash(textData);
                    return BitConverter.ToString(hash).Replace("-", String.Empty);
                }
            }
        }
    }
}