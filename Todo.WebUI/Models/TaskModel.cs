﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Web.Mvc;
using Todo.Entities;

namespace Todo.WebUI.Models
{
    public class TaskModel
    {
        [Display(Name = "Id")]
        [Required(ErrorMessage = "Id can not be empty")]
        public int Id { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessage = "Task can not be empty")]
        [StringLength(128, ErrorMessage = "Your task is too long")]
        [DataType(DataType.Text)]
        public string Title { get; set; }

        public bool IsDone { get; set; }

        [Required(ErrorMessage="You shoud choose priority")]
        [Range(1, 4, ErrorMessage="Priority can be only beetween 1 and 4")]
        public Priority Priority { get; set; }

        public IEnumerable<SelectListItem> PriorityList
        {
            get
            {
                yield return new SelectListItem { Text = Priority.Low.ToString(), Value = ((int)Priority.Low).ToString() };
                yield return new SelectListItem { Text = Priority.Medium.ToString(), Value = ((int)Priority.Medium).ToString() };
                yield return new SelectListItem { Text = Priority.High.ToString(), Value = ((int)Priority.High).ToString() };
                yield return new SelectListItem { Text = Priority.Critical.ToString(), Value = ((int)Priority.Critical).ToString() };
            }
        }
    }

}