﻿using System.Web.Mvc;
using Todo.Entities;
using Todo.Repositories;
using Todo.WebUI.Models;
using System.Collections.Generic;
using System;

namespace Todo.WebUI.Controllers
{
    public class TaskController : Controller
    {
        private readonly ITaskRepository _repository;

        public TaskController(ITaskRepository repository)
        {
            this._repository = repository;
        }

        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.tasks = _repository.GetAll();
            return View();
        }
        
        [HttpPost]
        public ActionResult Index(string task, int priority)
        {
            if (string.IsNullOrWhiteSpace(task) == false)
            {
                TaskEntity entity = new TaskEntity
                {
                    Title = task,
                    IsDone = false,
                    Priority = (Priority)priority
                };
                _repository.Add(entity);
            }
            else
            {
                ModelState.AddModelError("", "Task can not be empty");
            }
            ViewBag.tasks = _repository.GetAll();
            return View();
        }

        [HttpPost]
        public ActionResult Delete(int taskId)
        {
            _repository.Delete(taskId);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            TaskEntity entity = _repository.GetTask(id);
            TaskModel model = new TaskModel
            {
                Id = entity.Id,
                Title = entity.Title,
                IsDone = entity.IsDone,
                Priority = entity.Priority
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult SaveChanges(TaskModel task, string priority)
        {
            if (ModelState.IsValid)
            {
                int p = Convert.ToInt32(priority);
                TaskEntity e = new TaskEntity
                {
                    Id = task.Id,
                    Title = task.Title,
                    IsDone = task.IsDone,
                    Priority = (Priority)p
                };
                _repository.Save(e);
            }
            else
            {
                ModelState.AddModelError("", "Task can not be empty");
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Cancel()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult SetStatus(int taskId, bool isDone)
        {
            TaskEntity task = _repository.GetTask(taskId);
            task.IsDone = isDone;
            _repository.Save(task);
            return Json(new object());
        }

        [HttpGet]
        public ActionResult FilterByStatus(bool? filterContext)
        {
            List<TaskEntity> filteredList = new List<TaskEntity>();
            if(filterContext != null)
            {
                filteredList = _repository.FilterByStatus((bool)filterContext);
            }
            else
            {
                filteredList = _repository.GetAll();
            }
            return PartialView("FilteredListPartial", filteredList);                
        }

        [HttpGet]
        public ActionResult FilterByPriority(int? priorityValue)
        {
            List<TaskEntity> filteredList = new List<TaskEntity>();
            if (priorityValue != null)
            {
                filteredList = _repository.FilterByPriority((Priority)priorityValue);
            }
            else
            {
                filteredList = _repository.GetAll();
            }
            return PartialView("FilteredListPartial", filteredList);
        }

        [HttpGet]
        public ActionResult SortByPriority(bool? param)
        {
            var sortedList = new List<TaskEntity>();
            if(param != null)
            {
                sortedList = _repository.SortByPriority((bool)param);
            }
            else
            {
                sortedList = _repository.GetAll();
            }
            return PartialView("FilteredListPartial", sortedList);
        }

        [HttpGet]
        public ActionResult Sort(bool? param)
        {
            var filteredList = new List<TaskEntity>();
            if (param != null)
            {
                filteredList = _repository.GetSortedList((bool)param);
            }
            else
            {
                filteredList = _repository.GetAll();
            }
            return PartialView("FilteredListPartial", filteredList);
        }
    }
}