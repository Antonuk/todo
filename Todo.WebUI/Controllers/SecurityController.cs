﻿using System.Web.Mvc;
using Todo.Entities;
using Todo.WebUI.Code.Manager;
using Todo.WebUI.Models;

namespace Todo.WebUI.Controllers
{
    public class SecurityController : Controller
    {
        private ISecurityManager _manager;

        public SecurityController(ISecurityManager manager)
        {
            this._manager = manager;
        }
        // GET: /Security/
        [HttpGet]
        public ActionResult Login()
        {
            if(_manager.IsAuthentificated())
            {
                return RedirectToAction("Index", "Task");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel loginModel)
        {
            if (_manager.Login(loginModel.Name, loginModel.Password))
            {
                return RedirectToAction("Index", "Task");
            }
            else
            {
                /*
                if (loginModel.Name != "sa@mail.it")
                {
                    ModelState.AddModelError("", "Email doesn't match");
                }
                if (loginModel.Password != "sysAd123")
                {
                    ModelState.AddModelError("", "Password doesn't match");
                }*/
                ModelState.AddModelError("", "Login name or password are not correct");
                return View();
            }
        }

        [HttpPost]
        public ActionResult Logout()
        {
            _manager.Logout();
            return RedirectToAction("Login", "Security");
        }
    }
}