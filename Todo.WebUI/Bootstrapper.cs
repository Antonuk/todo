using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc4;
using Todo.Repositories;
using System.Configuration;
using Todo.WebUI.Code.Manager;

namespace Todo.WebUI
{
  public static class Bootstrapper
  {
    public static IUnityContainer Initialise()
    {
      var container = BuildUnityContainer();

      DependencyResolver.SetResolver(new UnityDependencyResolver(container));

      return container;
    }

    private static IUnityContainer BuildUnityContainer()
    {
      var container = new UnityContainer();

      // register all your components with the container here
      // it is NOT necessary to register your controllers

      // e.g. container.RegisterType<ITestService, TestService>();    
      RegisterTypes(container);
      return container;
    }

    public static void RegisterTypes(IUnityContainer container)
    {
        string connectionString = ConfigurationManager.ConnectionStrings["TodoDatabase"].ConnectionString;
        container.RegisterType<ITaskRepository, SqlTaskRepository>(new InjectionConstructor(connectionString));
        //container.RegisterType<IUserRepository, FakeUserRepository>();
        container.RegisterType<IUserRepository, SqlUserRepository>(new InjectionConstructor(connectionString));
        container.RegisterType<ISecurityManager, FormsSecurityManager>();
    }
  }
}