﻿(function () {

    var clickCounter = null;
    var clickStatus = 0; // no sort parameters

    var onSortClick = function (e) {
        $el = $(e.target);
        console.log("Counter = " + clickCounter);
        console.log("Status = " + clickStatus);
        clickStatus++;

        //sort by desc
        if (clickStatus == 1) {
            clickCounter = true;
            $el.html("Status &#8593;");
        }
        //sort by asc
        else if (clickStatus == 2) {
            clickCounter = false;
            $el.html("Status &#8595;");
        }
        //no sort param
        else {
            clickCounter = null;
            clickStatus = 0;
            $el.html("Status");
        }

        $.ajax({
            url: "/Task/Sort",
            type: "get",
            dataType: "html",
            data: {
                param: clickCounter
            }
        }).done(function (data) {
            console.log("click sorting Ajax is done");
            $("#tasks").html(data);
            $(".cbx").on("click", onCheckBoxClick);
        }).fail(function () {
            console.log("click sorting Ajax is FAIL");
        });
    }

    var priorityCounter = 0;
    var priorityStatus = null;
    var onPrioritySortClick = function (e) {
        $el = $(e.target);
        console.log("Counter = " + priorityCounter);
        console.log("Status = " + priorityStatus);
        
        priorityCounter++;
        
        //sort by desc
        if (priorityCounter == 1) {
            priorityStatus = true;
            $el.html("Priority &#8593;");
        }
        //sort by asc
        else if (priorityCounter == 2) {
            priorityStatus = false;
            $el.html("Priority &#8595;");
        }
        //no sort param
        else {
            priorityStatus = null;
            priorityCounter = 0;
            $el.html("Priority");
        }
        
        $.ajax({
            url: "/Task/SortByPriority",
            type: "get",
            dataType: "html",
            data: {
                param: priorityStatus
            }
        }).done(function (data) {
            console.log("click sorting priority Ajax is done");
            $("#tasks").html(data);
            $(".cbx").on("click", onCheckBoxClick);
        }).fail(function () {
            console.log("click sorting priority Ajax is FAIL");
        });
    }

    var onCheckBoxClick = function (e) {
        var $el = $(e.target);
        var id = $el.data("taskId");
        var done = $el.is(":checked");

        console.log("Id is " + id + " done " + done);

        $.ajax({
            url: "/Task/SetStatus",
            type: "post",
            dataType: "json",
            data : {
                taskId : id,
                isDone : done
            }
        }).done(function () {
            console.log("ajax click is done");
        }).fail(function() {
            console.log("ajax click is FAIL");
        });
    }

    var onSelectFilter = function (e) {
        $el = $(e.target);
        var filter = $el.find("option:selected").attr("value");
        console.log("Filter = " + filter);

        $.ajax({
            type: "get",
            url: "/Task/FilterByStatus",
            dataType: "html",
            data: "filterContext=" + e.target.value
        }).done(function (data) {
            console.log("ajax filter is done!" + data);
            $("#tasks").html(data);
            init();
        }).fail(function () {
            console.log("ajax filter is FAIL");
        })
    }

    var onSelectPriority = function (e)
    {
        $el = $(e.target);
        var prio = $el.find("option:selected").attr("value");
        console.log("Priority = " + prio);

        $.ajax({
            type: "get",
            url: "/Task/FilterByPriority",
            dataType: "html",
            data: "priorityValue=" + e.target.value
        }).done(function (data) {
            console.log("ajax priority filter is done!" + data);
            $("#tasks").html(data);
            init();
        }).fail(function () {
            console.log("ajax filter is FAIL");
        })
    }

    var init = function () {
        $(".cbx").on("click", onCheckBoxClick);
        console.log("Checkbox init is fine");
    }

    var initStatusFilter = function () {
        $("#selecter").on("change", onSelectFilter);
        console.log("Status filter init is fine");
    }

    var initPriorityFilter = function () {
        $("#priority").on("change", onSelectPriority);
        console.log("Priority filter init is fine");
    }

    var initSort = function () {
        $(".sort").on("click", onSortClick);
        console.log("sort init is fine");
    }
    var initPrioritySort = function () {
        $(".priority").on("click", onPrioritySortClick);
        console.log("Sort by Priority init is OK");
    }

    $(function () {
        init();
        initPriorityFilter();
        initStatusFilter();
        initSort();
        initPrioritySort();
        console.log("Task.js is loaded");
    });
})();