﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Todo.Entities;
using System.Linq;

namespace Todo.Repositories
{
    public class SqlTaskRepository : ITaskRepository
    {
        private readonly string _connectionString;
        private const string SELECT_TASK = "SELECT [Id], [Title], [IsDone], [Priority] FROM [dbo].[Task]";

        public SqlTaskRepository(string connectionString)
        {
            this._connectionString = connectionString;
        }

        public List<TaskEntity> GetAll()
        {
            using (var connection = new SqlConnection(this._connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = SELECT_TASK;
                    using (var reader = command.ExecuteReader())
                    {
                        List<TaskEntity> taskList = new List<TaskEntity>();
                        while(reader.Read())
                        {
                            TaskEntity task = new TaskEntity
                            {
                                Id = (int) reader["Id"],
                                Title = (string) reader["Title"],
                                IsDone = (bool) reader["IsDone"],
                                Priority = (Priority) reader["Priority"]
                            };
                            taskList.Add(task);
                        }
                        return taskList;
                    }
                }
            }
        }
       
        public void Add(TaskEntity task)
        {
            int isDone = (task.IsDone) ? 1 : 0;
            using (var connection = new SqlConnection(this._connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {                    
                    command.Connection = connection;
                    command.Parameters.AddWithValue("@Title", task.Title);
                    command.Parameters.AddWithValue("@IsDone", isDone);
                    command.Parameters.AddWithValue("@Priority", task.Priority);
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "INSERT INTO [dbo].[Task]([Title], [IsDone], [Priority]) VALUES (@Title, @IsDone, @Priority);";
                    command.ExecuteNonQuery();
                }
            }
        }

        public void Delete(int id)
        {
            using(var connection = new SqlConnection(this._connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.Parameters.AddWithValue("@Id", id);
                    command.CommandText = "DELETE FROM [dbo].[Task] WHERE [Id] = @Id;";
                    command.ExecuteNonQuery();
                }
            }           
        }      

        public TaskEntity GetTask(int id)
        {
            using (var connection = new SqlConnection(this._connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.Parameters.AddWithValue("@Id", id);
                    command.CommandText = SELECT_TASK + " WHERE [Id] = @Id;";
                    using (var reader = command.ExecuteReader())
                    {
                        TaskEntity entity = new TaskEntity();
                        while (reader.Read())
                        {
                            entity.Id = (int)reader["Id"];
                            entity.Title = (string)reader["Title"];
                            entity.IsDone = (bool)reader["IsDone"];
                            entity.Priority = (Priority)reader["Priority"];
                        }
                        return entity;
                    }
                } 
            }
        }

        public void Save(TaskEntity entity)
        {
            int isDone = (entity.IsDone) ? 1 : 0;
            int priority = Convert.ToInt32(entity.Priority);
            using (var connection = new SqlConnection(this._connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.Parameters.AddWithValue("@Title", entity.Title);
                    command.Parameters.AddWithValue("@IsDone", isDone);
                    command.Parameters.AddWithValue("@Id", entity.Id);
                    command.Parameters.AddWithValue("@Priority", priority);
                    command.CommandText = "UPDATE [dbo].[Task] SET [Title] = @Title, [IsDone] = @IsDone, [Priority] = @Priority WHERE [Id] = @Id;";
                    command.ExecuteNonQuery();
                }
            }
        }


        public List<TaskEntity> GetSortedList(bool parameter)
        {
            var filteredList = (from e in GetAll()
                                orderby e.IsDone == parameter
                                select e).ToList();
            return filteredList;
        }

        public List<TaskEntity> FilterByStatus(bool status)
        {
            return GetAll().FindAll(t => t.IsDone == status).ToList();
        }

        public List<TaskEntity> FilterByPriority(Priority priority)
        {
            return GetAll().FindAll(t => t.Priority == priority);
        }

        public List<TaskEntity> SortByPriority(bool sortOption)
        {
            var sortedList = new List<TaskEntity>();
            if(sortOption)
            {
                sortedList = (from e in GetAll()
                              orderby e.Priority descending
                              select e).ToList();
            }
            else
            {
                sortedList = (from e in GetAll()
                              orderby e.Priority ascending
                              select e).ToList();
            }
            return sortedList;
        }
    }
}
