﻿using System;
using System.Data.SqlClient;
using Todo.Entities;

namespace Todo.Repositories
{
    public class SqlUserRepository : IUserRepository
    {
        private readonly string _connectionString;

        public SqlUserRepository(string connectionString)
        {
            this._connectionString = connectionString;
        }

        public bool Login(string email, string password)
        {
            try
            {
                var user = GetUserByEmail(email);
                if (user.Email == email && user.Password == password)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch(ArgumentNullException e)
            {
                Console.WriteLine(e.Message); //?
                return false;
            }
           
        }

        private UserEntity GetUserByEmail(string email)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.Parameters.AddWithValue("@Email", email);
                    command.CommandText = "SELECT [Id], [Name], [Email], [Pass] FROM [dbo].[User] WHERE Email = @Email;";
                    using (var reader = command.ExecuteReader())
                    {
                        var user = new UserEntity();
                        while (reader.Read())
                        {
                            user.Id = (int)reader["Id"];
                            user.Name = (string)reader["Name"];
                            user.Email = (string)reader["Email"];
                            user.Password = (string)reader["Pass"];
                        }
                        if (user != null)
                        {
                            return user;
                        }
                        else
                        {
                            throw new ArgumentNullException("User doesn't exist");
                        }
                    }
                }
            }
        }
    }
}