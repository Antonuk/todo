﻿using System.Collections.Generic;
using Todo.Entities;

namespace Todo.Repositories
{
    public interface ITaskRepository
    {
        void Add(TaskEntity task);
        void Delete(int id);
        void Save(TaskEntity entity);
        TaskEntity GetTask(int id);
        List<TaskEntity> GetAll();
        List<TaskEntity> GetSortedList(bool parameter);
        List<TaskEntity> FilterByStatus(bool status);
        List<TaskEntity> FilterByPriority(Priority priority);
        List<TaskEntity> SortByPriority(bool sortOption);
    }
}