﻿namespace Todo.Repositories
{
    public interface IUserRepository
    {
        bool Login(string email, string password);
    }
}
