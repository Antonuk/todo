﻿using System;
using System.Collections.Generic;
using Todo.Entities;

namespace Todo.Repositories
{
    public class FakeTaskRepository : ITaskRepository
    {
        private static List<TaskEntity> _taskList;
        
        static FakeTaskRepository() 
        {
            _taskList = new List<TaskEntity>();
            _taskList.Add(new TaskEntity { Id = 1, Title = "Task1", IsDone = false, Priority = (Priority)1 });
            _taskList.Add(new TaskEntity { Id = 2, Title = "Task2", IsDone = true, Priority = (Priority)2 });
            _taskList.Add(new TaskEntity { Id = 3, Title = "Task3", IsDone = false, Priority = (Priority)4 });
            _taskList.Add(new TaskEntity { Id = 4, Title = "Task4", IsDone = true, Priority = (Priority)4 });
        }

        public List<TaskEntity> GetAll()
        {           
            return _taskList;
        }

        public void Add(TaskEntity task)
        {
            _taskList.Add(task);
        }

        public void Delete(int id)
        {
            _taskList.RemoveAll(e => e.Id == id);
        }

        public TaskEntity GetTask(int id)
        {
            return _taskList.Find(e => e.Id == id);
        }

        public void Save(TaskEntity entity)
        {
            TaskEntity task = _taskList.Find(e => e.Id == entity.Id);
            task.Title = entity.Title;
            task.IsDone = entity.IsDone;
            task.Priority = entity.Priority;
        }
        public List<TaskEntity> GetSortedList(bool parameter)
        {
            throw new NotImplementedException();
        }

        public List<TaskEntity> FilterByStatus(bool status)
        {
            throw new NotImplementedException();
        }

        public List<TaskEntity> FilterByPriority(Priority priority)
        {
            throw new NotImplementedException();
        }

        public List<TaskEntity> SortByPriority(bool sortOption)
        {
            throw new NotImplementedException();
        }
    }
}